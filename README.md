# tensentric-interview-24

This program was written and designed by Jackie Herzberg for the Tensentric 2024 Associateship Interview. Thank you to Jordan Stockdale, Tessa Rowe, and Dante Pasionek for this opportunity! This firmware was designed for the nRF52833 (nRF Connect SDK v2.4.0).

# Description

This program was developed to interact with a hardware development kit (nRF52833). It utilizes a state machine approach with the Zephyr state machine framework. Each individual state assumes a specific task for the purpose of the Intern Take Home Project. A state diagram is included to provide a general overview of the workflow of the program as well as a logging_output.txt file to provide an example output of the implemented logging. This logging was utilized as validation for the success of specific modules of the program. 

The default state of the program is in "idle" which ultimately waits until a button is pressed to perform the functional process (placing pucks, assigning parking spots, closing gaps, popping pucks, performing work(), etc.). This was implemented to conserve energy so the device is not running when unnecessary. Once the process is complete, the system will return to idle and similarly hault any function to wait for a button press again. The work() function is represented with a timer that blinks an LED on the DK while it's running.

# Future Work

I am a firm believer that no project is completely finished, there's always room for improvement! With additional time, there are specific aspects of the project that I would improve further. Specifically, with regard to testing, I would implement the Zephyr unit testing framework to test specific modules of my program. To do so, I would create separate header and code files with the specific functions and states that I could run individual tests on. To make some of my program "more testable," I could alter each function used to have specific inputs and outputs, which the unit testing would modulate to evaluate the results. However, utilizing global variables optimizes storage on the development kit, so the ability of the functions to run without using additional memory is ideal.

In addition to unit testing, I would implement exception handling cases. For example, tests to determine if the inputs are out of bounds and then how to proceed with that data. Another example could be two pucks starting at the same random location. Similarly, I could include exit codes to each state and function in the case that an unexpected behavior occurs. An "error" state could be created which would shut down function of the device until it was reset.  