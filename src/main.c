#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

/*General Libraries*/
#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/logging/log.h>
#include <zephyr/smf.h>

LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

#define HEARTBEAT_MS 500 //Heartbeat frequency; in ms
#define SLEEP_TIME_MS 100 //Sleep in ms
#define MIN_PUCKS 1 //Minimum # of pucks
#define MAX_PUCKS 9 //Maximum # of pucks 
#define MAX_X 480 //Maximum X coordinate
#define MIN_X 0 //Minimum X coordinate
#define MAX_Y 480 //Maximum Y coordinate
#define MIN_Y 0 //Minimum Y coordinate
#define TEST_SIZE 5
#define WORK_LED_MS 100 //Frequency of work() LED
#define WORK_ONESHOT_MS 3000 //Duration of work() LED
#define ROWS 2

/*Events*/
#define PUCK_EVENT BIT(0)

/*Initialize GPIO Structs for LEDs*/
static const struct gpio_dt_spec heartbeat_led = GPIO_DT_SPEC_GET(DT_ALIAS(heartbeat), gpios);
static const struct gpio_dt_spec work_led = GPIO_DT_SPEC_GET(DT_ALIAS(workled), gpios);

/*Initialize GPIO Structs for Buttons*/
static const struct gpio_dt_spec puck_button = GPIO_DT_SPEC_GET(DT_ALIAS(button1), gpios);

/* State Machine Stuff */
/* Forward declaration of state table */
static const struct smf_state user_states[];

/* List of demo states */
enum user_state { init, idle, gen_pucks, place_pucks, work };

/* User defined object */
struct s_object {
        struct smf_ctx ctx;

        struct k_event smf_event;
        int32_t events;
} state_obj;

/*Define Structs*/
struct puck_vars {
    uint8_t num_pucks;
    uint16_t pos_pucks;
    bool work_completed;
    int previous_popped_puck;
    int previous_puck_index;
    int first_puck;
} puck_vars;

struct puck_pos {
    uint16_t x_coord[MAX_PUCKS];
    uint16_t y_coord[MAX_PUCKS];
} puck_pos; 

struct parking_coord {
    uint16_t x_coord[MAX_PUCKS];
    uint16_t y_coord[MAX_PUCKS];  
    bool spot_used[MAX_PUCKS];
};

/*Initialize Known Parking Spot Coordinates*/
struct parking_coord parking_pos = {
    .x_coord = {180, 300, 420, 420, 300, 180, 180, 300, 420},
    .y_coord = {60, 60, 60, 180, 180, 180, 300, 300, 300},
    .spot_used = {0}
}; 

int min_distance_spot[ROWS][MAX_PUCKS];
int parking_puck_number[MAX_PUCKS] = {0};
bool spots_occupied[MAX_PUCKS] = {0};
uint16_t parking_coord_copy[ROWS][MAX_PUCKS] = {0};

enum parking_spots {x_coord_ps, y_coord_ps};

/*Initialize Callbacks*/
static struct gpio_callback puck_button_cb;

/*Declare General Functions*/
int check_gpio_interfaces(void);
int config_leds(void);
int config_buttons(void);
void initialize_leds(void);

/*Declare Calculation Functions*/
void calculate_distance(uint16_t puck_xcoord, uint16_t puck_ycoord, int ind);
void calculate_absolute_min(void);
int check_zeros(int array[1][MAX_PUCKS], int size);
void shift_close_gaps(void);
void shift_pucks(void);
void check_puck_position(void);
void initialize_parking_spots(void);
void pop_puck_work(void);

/*Declare Callback Function*/
void puck_button_callback(const struct device *dev, struct gpio_callback *cb, uint32_t pins);

/*Declare Timer Functions*/
void heartbeat_start(struct k_timer *heartbeat_timer);
void work_timer_handler(struct k_timer *work_timer);
void work_stop(struct k_timer *work_timer);
void work_oneshot(struct k_timer *work_oneshot_timer);

/*Declare Timers*/
K_TIMER_DEFINE(heartbeat_timer, heartbeat_start, NULL);
K_TIMER_DEFINE(work_timer, work_timer_handler, work_stop);
K_TIMER_DEFINE(work_oneshot_timer, work_oneshot, NULL);

/**** STATE: Init ****/
static void init_run(void *o) {
	LOG_INF("STATE: Init (Run)");

    int err = check_gpio_interfaces();
	if (err) {
		LOG_ERR("GPIO interfaces not ready (err = %d)", err);
	}

    err = config_leds();
	if (err) {
			LOG_ERR("LED pin configuration error (%d).", err);
	}

	err = config_buttons();
	if (err) {
			LOG_ERR("Button configuration error (%d).", err);
	}

    smf_set_state(SMF_CTX(&state_obj), &user_states[idle]);
}

static void init_exit(void *o) {
	LOG_INF("STATE: Init (Exit)");

    initialize_leds();

    k_timer_start(&heartbeat_timer, K_MSEC(HEARTBEAT_MS), K_MSEC(HEARTBEAT_MS));
}

/**** STATE: Idle ****/
static void idle_entry(void *o) {
	LOG_INF("STATE: Idle (Entry)");

    /*Initialize Variables to Zero*/
    puck_vars.num_pucks = 0;
    puck_vars.pos_pucks= 0;
    puck_vars.work_completed= 0;
    puck_vars.previous_popped_puck= 0;
    puck_vars.previous_puck_index= 0;
    puck_vars.first_puck= 0;

    for (int i = 0; i<MAX_PUCKS; i++) {
        parking_puck_number[i] = 0;
        spots_occupied[i] = 0;
        parking_pos.spot_used[i] = 0;
        puck_pos.x_coord[i] = 0;
        puck_pos.y_coord[i] = 0;
        min_distance_spot[0][i] = 0;
        min_distance_spot[1][i] = 0;
    }

    /*Re-Initialize Parking Spots*/
    initialize_parking_spots();

    /*Enable Button Press with Callback*/
	gpio_pin_interrupt_configure_dt(&puck_button, GPIO_INT_EDGE_TO_ACTIVE);
	gpio_init_callback(&puck_button_cb, puck_button_callback, BIT(puck_button.pin));
	gpio_add_callback(puck_button.port, &puck_button_cb);
}

static void idle_run(void *o) {
	LOG_INF("STATE: Idle (Run)");

    /* Block until an event is detected */
    state_obj.events = k_event_wait(&state_obj.smf_event, PUCK_EVENT, true, K_FOREVER);
    if (state_obj.events) {
	 	LOG_DBG("Trigger switch to gen_pucks state.");
        smf_set_state(SMF_CTX(&state_obj), &user_states[gen_pucks]);
        state_obj.events = 0;
    }
}

/**** STATE: Gen Pucks ****/
static void gen_pucks_run(void *o) {
	LOG_INF("STATE: Gen Pucks (Run)");

    /*"Pseudo-Randomly" Generate # Pucks*/
    puck_vars.num_pucks = TEST_SIZE; //Start with static # of pucks for testing purposes

    smf_set_state(SMF_CTX(&state_obj), &user_states[place_pucks]);
}

static void gen_pucks_exit(void *o) {
	LOG_INF("STATE: Gen Pucks (Exit)");

    /*"Pseudo-Randomly" Assign X and Y Coordinates*/
    puck_pos.x_coord[0] = 60;
    puck_pos.x_coord[1] = 60;
    puck_pos.x_coord[2] = 60;
    puck_pos.x_coord[3] = 325;
    puck_pos.x_coord[4] = 400; 
    //puck_pos.x_coord[4] = 100;
    //puck_pos.x_coord[5] = 100; //Need to change TEST_SIZE to add pucks
    //puck_pos.x_coord[6] = 460;

    puck_pos.y_coord[0] = 420;
    puck_pos.y_coord[1] = 300;
    puck_pos.y_coord[2] = 180;
    puck_pos.y_coord[3] = 200;
    puck_pos.y_coord[4] = 100;
    //puck_pos.y_coord[4] = 100;
    //puck_pos.y_coord[5] = 100; //Need to change TEST_SIZE to add pucks
    //puck_pos.y_coord[6] = 200;

    for (int i=0; i<MAX_PUCKS; i++) {
        LOG_INF("Puck Pos: (%d, %d)", puck_pos.x_coord[i], puck_pos.y_coord[i]);
    } 
}

/**** STATE: Place Pucks ****/
static void place_pucks_entry(void *o) {
	LOG_INF("STATE: Place Pucks (Entry)");

    puck_vars.first_puck = 0;

    /*Calculate Distance for Each Puck; Store Minimum Distance*/
    for (int i = 0; i<puck_vars.num_pucks; i++) {
        calculate_distance(puck_pos.x_coord[i], puck_pos.y_coord[i], i);
        k_msleep(100);
    }

    /*Print the Minimum Distances of Pucks to Spots*/
    for (int i = 0; i < MAX_PUCKS; i++) {
        LOG_INF("Min Distance: %d, Spot #: %d", min_distance_spot[0][i], min_distance_spot[1][i]);
    }
}

static void place_pucks_run(void *o) {
	LOG_INF("STATE: Place Pucks (Run)");

    /*Find Minimum Distance of ALL Pucks*/
    calculate_absolute_min(); //Park first puck
    
    /*Repeat Process Until All Pucks are Parked*/
    for (int i = 0; i < MAX_PUCKS; i++) {
        LOG_INF("Min Distance: %d, Spot #: %d", min_distance_spot[0][i], min_distance_spot[1][i]);
    } //Print New Distances Array

    k_msleep(SLEEP_TIME_MS);

    if (check_zeros(min_distance_spot, MAX_PUCKS)) {
        LOG_INF("All Pucks are Parked!");

        smf_set_state(SMF_CTX(&state_obj), &user_states[work]);
    }
}

static void place_pucks_exit(void *o) {
 	LOG_INF("STATE: Place Pucks (Exit)");

    /*Close Gaps*/
    for (int i = 0; i < MAX_PUCKS; i++) {
        spots_occupied[i] = parking_pos.spot_used[i];
    }
    
    /*Re-Initialize Parking Spots with Initial Coordinates*/ 
    initialize_parking_spots();

    for (int i=0; i < MAX_PUCKS; i++) {
        LOG_INF("Puck Pos: (%d, %d)", puck_pos.x_coord[i], puck_pos.y_coord[i]);
    } //Reprint with Parked Coordinates

    for (int i = 0; i < MAX_PUCKS; i++) {
        LOG_INF("Parking Spot #: %d, Taken (y/n): %d, Puck #: %d", i+1, spots_occupied[i], parking_puck_number[i]);
    } //Print Parking Spots

    /*Go Through Occupied Spots and Shift Accordingly*/
    shift_close_gaps();

    for (int i = 0; i < MAX_PUCKS; i++) {
        LOG_INF("Parking Spot #: %d, Taken (y/n): %d, Puck #: %d", i+1, spots_occupied[i], parking_puck_number[i]);
    } //Print Parking Spots

    /*Store New Coordinates for Shifted Pucks*/
    for (int i = 0; i < MAX_PUCKS; i++) {
        if (spots_occupied[i]) {
            puck_pos.x_coord[parking_puck_number[i]-1] = parking_coord_copy[x_coord_ps][i];
            puck_pos.y_coord[parking_puck_number[i]-1] = parking_coord_copy[y_coord_ps][i];
        }
    }

    puck_vars.first_puck = parking_puck_number[MAX_PUCKS-1]; //Store puck # in the first spot
}

/**** STATE: Work ****/
static void work_entry(void *o) {
	LOG_INF("STATE: Work (Entry)");

    puck_vars.previous_popped_puck = 0;
    puck_vars.previous_puck_index = 0;
    puck_vars.work_completed = false;
}

static void work_run(void *o) {
	LOG_INF("STATE: Work (Run)");

    /*Re-Initialize Parking Spots with Initial Coordinates*/
    initialize_parking_spots();

    /*Check if there is a Puck at the First Spot (420, 300); Perform work on puck; reset spots_occupied and puck_number*/
    pop_puck_work();

    /*Shift other pucks while work() is being performed*/
    shift_pucks();

    /*Store New Coordinates for Shifted Pucks*/
    for (int i = 0; i < MAX_PUCKS; i++) {
        if (spots_occupied[i]) {
            puck_pos.x_coord[parking_puck_number[i]-1] = parking_coord_copy[x_coord_ps][i];
            puck_pos.y_coord[parking_puck_number[i]-1] = parking_coord_copy[y_coord_ps][i];
        }
    } 

    for (int i=0; i < MAX_PUCKS; i++) {
        LOG_INF("Puck #%d, Pos: (%d, %d)", (i+1), puck_pos.x_coord[i], puck_pos.y_coord[i]);
    } //Reprint with Shifted Coordinates

    k_msleep(WORK_ONESHOT_MS);

    /*Once work() is done, move popped puck to the first spot (180,60)*/
    if (puck_vars.work_completed) { //When work is done, turn a boolean ON
        puck_pos.x_coord[puck_vars.previous_puck_index] = parking_coord_copy[x_coord_ps][0]; 
        puck_pos.y_coord[puck_vars.previous_puck_index] = parking_coord_copy[y_coord_ps][0];
        spots_occupied[0] = 1;
        parking_puck_number[0] = puck_vars.previous_popped_puck;

        LOG_INF("Popped Puck Returned to Spot 1");
        puck_vars.work_completed = false;
        //return popped puck to the first spot
    }

    for (int i = 0; i < MAX_PUCKS; i++) {
        LOG_INF("Parking Spot #: %d, Taken (y/n): %d, Puck #: %d", i+1, spots_occupied[i], parking_puck_number[i]);
    } 

    for (int i=0; i < MAX_PUCKS; i++) {
        LOG_INF("Puck #%d, Pos: (%d, %d)", (i+1), puck_pos.x_coord[i], puck_pos.y_coord[i]);
    } //Reprint with Shifted Coordinates

    /*Check if Pucks are back in initial positions*/
    check_puck_position();
}


void main(void) {

    int err;

    /*Initialize Event Framework*/
    k_event_init(&state_obj.smf_event);

    /*Set Initial State*/
    smf_set_initial(SMF_CTX(&state_obj), &user_states[init]);

    while (1) {
        
        err = smf_run_state(SMF_CTX(&state_obj));
        if (err) {
            break;
        }

        k_msleep(SLEEP_TIME_MS);
    }
}

/*Define General Functions*/
int check_gpio_interfaces(void) {
        if (!device_is_ready(heartbeat_led.port)) {
		LOG_ERR("gpio0 interface not ready.");
		return -1;
	}

    return 0;
}

int config_leds(void) {
    int err;

        err = gpio_pin_configure_dt(&heartbeat_led, GPIO_OUTPUT_ACTIVE);
	if (err < 0) {
		LOG_ERR("Cannot configure heartbeat LED.");
		return err;
	}

	err = gpio_pin_configure_dt(&work_led, GPIO_OUTPUT);
	if (err < 0) {
		LOG_ERR("Cannot configure work led pin.");
	}

    return err;
}

int config_buttons(void) {
    int err;

    /* Initialize Button GPIO pins to Inputs */
	err = gpio_pin_configure_dt(&puck_button, GPIO_INPUT);
	if (err < 0) {
		LOG_ERR("Cannot configure button pin.");
	}

    return err;
}

void initialize_leds(void){
	/*Initialize LEDS to OFF*/
	gpio_pin_set_dt(&heartbeat_led, 0);
	gpio_pin_set_dt(&work_led, 0);
}

/*Define Callback Function*/
void puck_button_callback(const struct device *dev, struct gpio_callback *cb, uint32_t pins){
    LOG_INF("Button 1 Callback");
    k_event_post(&state_obj.smf_event, PUCK_EVENT);
}

/*Define Timer Functions*/
void heartbeat_start(struct k_timer *heartbeat_timer) {
    gpio_pin_toggle_dt(&heartbeat_led);
}

/*Define Calculation Functions*/
void calculate_distance(uint16_t puck_xcoord, uint16_t puck_ycoord, int ind) {
    /*Find Distance Between Puck and Parking Spots*/
    int x_val[MAX_PUCKS] = {0};
    int y_val[MAX_PUCKS] = {0};
    int x_square[MAX_PUCKS] = {0};
    int y_square[MAX_PUCKS] = {0};
    int sum[MAX_PUCKS] = {0};
    float dist[MAX_PUCKS] = {0};
    int int_dist[MAX_PUCKS] = {0};

    /*Calculate distance of puck 1 from all parking spots*/
    for (int i = 0; i<MAX_PUCKS; i++) {
        x_val[i] = puck_xcoord - parking_coord_copy[x_coord_ps][i];
        y_val[i] = puck_ycoord - parking_coord_copy[y_coord_ps][i];
        x_square[i] = x_val[i] * x_val[i];
        y_square[i] = y_val[i] * y_val[i];
        sum[i] = x_square[i] + y_square[i];
        dist[i] = sqrtf((float)sum[i]);
        int_dist[i] = (int)dist[i];
        LOG_INF("x_val: %d, y_val: %d, sum: %d, dist: %d", x_val[i], y_val[i], sum[i], int_dist[i]);
    }

    /*Find Smallest Distance for Each Puck*/
    int min = int_dist[0];
    int spot = 1;

    for (int i = 0; i < MAX_PUCKS; i++) {
        if (int_dist[i] < min) {
            min = int_dist[i];
            spot = i+1;
        }
    }

    LOG_INF("Min Distance to Parking Spot: %d", min);
    LOG_INF("Parking Spot #: %d", spot);

    min_distance_spot[0][ind] = min; //Store min distance for this puck
    min_distance_spot[1][ind] = spot; //Store min parking spot # for this puck
}

void calculate_absolute_min(void) {
    /*Find Minimum Distance of ALL Pucks*/
    int min = min_distance_spot[0][0];
    int spot_num = min_distance_spot[1][0];
    int closest_puck_num = 1;

    for (int i = 0; i < MAX_PUCKS; i++) {
        if ((min_distance_spot[0][i] < min) && (min_distance_spot[1][i] != 0)) { //Look for minimum value, but ignore values without parking spots stored
            min = min_distance_spot[0][i];
            spot_num = min_distance_spot[1][i];
            closest_puck_num = i+1; //Store which puck had the absolute minimum distance
        } //Find minimum distance and corresponding parking spot number
    }

    /*Check for Repeated Spot #s*/
    for (int i = 0; i < MAX_PUCKS; i++) {
        for (int j = i + 1; j < MAX_PUCKS; j++) {
            if (min_distance_spot[1][i] == min_distance_spot[1][j] && min_distance_spot[1][i] == spot_num) {
                // Found a repeated value
                LOG_WRN("Repeated Spot Num! #%d", spot_num);
                //Compare repeated distances and store absolute min
                if (min_distance_spot[0][i] < min_distance_spot[0][j]) {
                    min = min_distance_spot[0][i];
                    spot_num = min_distance_spot[1][i];
                    closest_puck_num = i+1; //Store which puck had the absolute minimum distance
                } if (min_distance_spot[0][j] < min_distance_spot[0][i]) {
                    min = min_distance_spot[0][j];
                    spot_num = min_distance_spot[1][j];
                    closest_puck_num = j+1; //Store which puck had the absolute minimum distance
                }
            }
        }

        if (parking_pos.spot_used[spot_num-1] == true) {
            LOG_WRN("Parking Spot Occupied");
            parking_coord_copy[x_coord_ps][spot_num-1] = 999;
            parking_coord_copy[y_coord_ps][spot_num-1] = 999; //Repopulate Coordinates for Occupied Parking Spot

            calculate_distance(puck_pos.x_coord[closest_puck_num-1], puck_pos.y_coord[closest_puck_num-1], (closest_puck_num-1));
            min = min_distance_spot[0][closest_puck_num-1];
            spot_num = min_distance_spot[1][closest_puck_num-1];
        }
    }

    /*Reset Min Distances to "Delete" Absolute Min*/
    min_distance_spot[0][closest_puck_num-1] = 0;
    min_distance_spot[1][closest_puck_num-1] = 0;

    LOG_INF("Absolute Min Dist: %d, Parking Number: %d, Puck #: %d", min, spot_num, closest_puck_num);

    /*Park the Closest Puck in the Respective Parking Spot*/
    puck_pos.x_coord[closest_puck_num-1] = parking_coord_copy[x_coord_ps][spot_num-1];
    puck_pos.y_coord[closest_puck_num-1] = parking_coord_copy[y_coord_ps][spot_num-1];
    parking_pos.spot_used[spot_num-1] = true;
    parking_puck_number[spot_num-1] = closest_puck_num;

    LOG_INF("New Coord for Puck #%d: (%d, %d)", closest_puck_num, puck_pos.x_coord[closest_puck_num-1], puck_pos.y_coord[closest_puck_num-1]);
}

int check_zeros(int array[1][MAX_PUCKS], int size) {
    for (int i = 0; i < size; i++) {
        if (array[1][i] != 0) {
            return 0;
        }
    }
    return 1;
}

void shift_close_gaps(void) {

    int lastNonZeroIndex = MAX_PUCKS-1;
    while (lastNonZeroIndex >= 0 && spots_occupied[lastNonZeroIndex] == 0) {
        lastNonZeroIndex--;
    }

    int destinationIndex =  MAX_PUCKS - 1;
    for (int i = lastNonZeroIndex; i >= 0; i--) {
        if (spots_occupied[i] != 0) {
            spots_occupied[destinationIndex] = spots_occupied[i];
            parking_puck_number[destinationIndex] = parking_puck_number[i];
            spots_occupied[i] = 0;
            parking_puck_number[i] = 0;
            destinationIndex--;
        }
    }

    while (destinationIndex >= 0) {
        spots_occupied[destinationIndex] = 0;
        parking_puck_number[destinationIndex] = 0;
        destinationIndex--;
    }

    LOG_INF("Parking Spots Shifted and Gaps Closed!");
}

void pop_puck_work(void){
    for (int i = 0; i < MAX_PUCKS; i++) {
        if (puck_pos.x_coord[i] == parking_pos.x_coord[MAX_PUCKS-1] && puck_pos.y_coord[i] == parking_pos.y_coord[MAX_PUCKS-1]) {
            LOG_INF("Puck #%d is Popped!", (i+1));
            
            /*Start Work(); Indicated with LED toggling function*/
            k_timer_start(&work_timer, K_MSEC(WORK_LED_MS), K_MSEC(WORK_LED_MS));
            k_timer_start(&work_oneshot_timer, K_MSEC(WORK_ONESHOT_MS), K_NO_WAIT);

            puck_pos.x_coord[i] = 999;
            puck_pos.y_coord[i] = 999; //99999 indicates work is being performed with puck
            spots_occupied[MAX_PUCKS-1] = 0; //Reset Spot #9 to ZERO or FALSE
            puck_vars.previous_popped_puck = parking_puck_number[MAX_PUCKS-1]; //Store the number of the most recently "popped" puck
            puck_vars.previous_puck_index = i;
            parking_puck_number[MAX_PUCKS-1] = 0; //Same with Puck Number
        }
    }
    for (int i = 0; i < MAX_PUCKS; i++) {
        LOG_INF("Parking Spot #: %d, Taken (y/n): %d, Puck #: %d", i+1, spots_occupied[i], parking_puck_number[i]);
    } 
}

void shift_pucks(void) {
    for (int i = MAX_PUCKS-1; i>0; i--) {
        spots_occupied[i] = spots_occupied[i-1];
        parking_puck_number[i] = parking_puck_number[i-1];
    }
    spots_occupied[0] = 0;
    parking_puck_number[0] = 0;

    LOG_INF("Pucks Shifted! New Configuration:");

    for (int i = 0; i < MAX_PUCKS; i++) {
        LOG_INF("Parking Spot #: %d, Taken (y/n): %d, Puck #: %d", i+1, spots_occupied[i], parking_puck_number[i]);
    } 
}

void check_puck_position(void) {
    if (puck_pos.x_coord[puck_vars.first_puck-1] == parking_pos.x_coord[MAX_PUCKS-1] && puck_pos.y_coord[puck_vars.first_puck-1] == parking_pos.y_coord[MAX_PUCKS-1]) {
    LOG_WRN("All Pucks Processed and Shifted!");

    for (int i = 0; i < MAX_PUCKS; i++) {
        LOG_INF("Parking Spot #: %d, Taken (y/n): %d, Puck #: %d", i+1, spots_occupied[i], parking_puck_number[i]);
    } 
    for (int i=0; i < MAX_PUCKS; i++) {
        LOG_INF("Puck #%d, Pos: (%d, %d)", (i+1), puck_pos.x_coord[i], puck_pos.y_coord[i]);
    }

    smf_set_state(SMF_CTX(&state_obj), &user_states[idle]);
    }
}

void initialize_parking_spots(void){
    for (int i = 0; i<MAX_PUCKS; i++) {
        parking_coord_copy[x_coord_ps][i] = parking_pos.x_coord[i];
        parking_coord_copy[y_coord_ps][i] = parking_pos.y_coord[i];
    }
}

/*Define Timer Functions*/
void work_timer_handler(struct k_timer *work_timer) {
    gpio_pin_toggle_dt(&work_led);
}

void work_stop(struct k_timer *work_timer) {
    gpio_pin_set_dt(&work_led, 0);
}

void work_oneshot(struct k_timer *work_oneshot_timer) {
    LOG_INF("Work Timer Stopped");
    puck_vars.work_completed = true;
    k_timer_stop(&work_timer);
}

/**** STATE TABLE ****/
static const struct smf_state user_states[] = {
        [init] = SMF_CREATE_STATE(NULL, init_run, init_exit),
        [idle] = SMF_CREATE_STATE(idle_entry, idle_run, NULL),
        [gen_pucks] = SMF_CREATE_STATE(NULL, gen_pucks_run, gen_pucks_exit),
        [place_pucks] = SMF_CREATE_STATE(place_pucks_entry, place_pucks_run, place_pucks_exit),
		[work] = SMF_CREATE_STATE(work_entry, work_run, NULL)
};
/*********************/